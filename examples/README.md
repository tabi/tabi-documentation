Name of Your Project
====================

Here is the space for a short description of the documents. Provide enough information to spark a reader's interest.


### relation to other projects

These files will have no relation to other projects. For your documentation files this might be different, so make sure to provide some context here. 


### content

This `examples` folder contains two recipes.

*`examples_cola_recipe.pdf` is an open source cola recipe, licensed under the [GPL](examples_cola_recipe.LICENSE).*

One of the rare recipes under GPL license. Retrieved on 2017-11-21 from [http://cube-cola.org/index.php?route=information/information&information_id=10](). I never tried it. Might taste horrible, for all I know.

*`examples_oles_salad.md` is in the public domain under the Creative Commons license [CC0](examples_oles_salad.LICENSE).*

Waiving as many right as possible, this recipe is in the public domain under the CC0 license. Do with it whatever you want, no string attached. This dish takes a bit of time to prepare, but tastes great.
